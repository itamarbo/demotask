<h1>Create A New task</h1>
<form method = 'post' action="{{action('TaskController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">What is the new task?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>

</form>
