<?php

use App\Task;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/task', function(){
    /*
    $tasks = Task::all(); // שולף את כל הרשומות מטבלת טאסקס
    foreach($tasks as $task){

        echo $task->title ."<br>";
        
    }
    */

    // one to many relationship
      $user = User::find(1);
      foreach($user->tasks as $task){
      echo  $task->title ."<br>";
    
    }

});

/*
// one to many relationship
Route::get('/tasks', function(){

    $user = User::find(1);

    foreach($user->tasks as $task){
      echo  $task->title ."<br>";
    
    }
});
*/

Route::get('/tasks', 'TaskController@index');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('tasks', 'TaskController')->middleware('auth');
Route::get('/mytasks', 'TaskController@mytasks');